<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/peternagy/Devel/revuca/megakemper2019-final/user/config/site.yaml',
    'modified' => 1556746290,
    'data' => [
        'title' => 'MEGAKEMPER 2019',
        'default_lang' => 'en',
        'author' => [
            'name' => 'KEMP REVÚCA',
            'email' => 'info@megakemper.sk'
        ],
        'taxonomies' => [
            0 => 'category',
            1 => 'tag'
        ],
        'metadata' => [
            'description' => 'MEGAKEMPER 2019.'
        ],
        'summary' => [
            'enabled' => true,
            'format' => 'short',
            'size' => 300,
            'delimiter' => '==='
        ],
        'blog' => [
            'route' => '/blog'
        ],
        'header' => [
            'title' => 'MEGAKEMPER<span>2019</span>',
            'description' => 'V ZRKADLE - KAŽDÝ DEŇ SA DOŇ POZERÁŠ, ALE ČO VLASTNE VIDÍŠ?',
            'freereg' => 'VOĽNÉ MIESTA: ',
            'buttons' => [
                0 => [
                    'text' => 'REGISTROVAŤ!',
                    'link' => '/#registracia',
                    'class' => 'learn-more-btn'
                ]
            ]
        ],
        'footer' => [
            'links' => NULL
        ]
    ]
];
