<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/peternagy/Devel/revuca/megakemper2019-final/user/config/plugins/form.yaml',
    'modified' => 1525892602,
    'data' => [
        'enabled' => true,
        'built_in_css' => false,
        'inline_css' => false,
        'refresh_prevention' => false,
        'client_side_validation' => true,
        'inline_errors' => false,
        'files' => [
            'multiple' => false,
            'limit' => 10,
            'destination' => 'self@',
            'avoid_overwriting' => false,
            'random_name' => false,
            'filesize' => 0,
            'accept' => [
                0 => 'image/*'
            ]
        ]
    ]
];
