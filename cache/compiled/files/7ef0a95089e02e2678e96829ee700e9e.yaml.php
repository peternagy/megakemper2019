<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://halcyon/halcyon.yaml',
    'modified' => 1555681440,
    'data' => [
        'enabled' => true,
        'color' => 'blue',
        'dropdown' => [
            'enabled' => false
        ]
    ]
];
