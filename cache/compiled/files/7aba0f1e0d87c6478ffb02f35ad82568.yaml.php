<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://megatheme/megatheme.yaml',
    'modified' => 1555684060,
    'data' => [
        'streams' => [
            'schemes' => [
                'theme' => [
                    'type' => 'ReadOnlyStream',
                    'prefixes' => [
                        '' => [
                            0 => 'user/themes/megatheme',
                            1 => 'user/themes/halcyon'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
