<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://error/error.yaml',
    'modified' => 1523527136,
    'data' => [
        'enabled' => true,
        'routes' => [
            404 => '/error'
        ]
    ]
];
