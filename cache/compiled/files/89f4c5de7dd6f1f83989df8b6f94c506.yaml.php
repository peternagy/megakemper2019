<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/peternagy/Devel/revuca/megakemper2019-final/user/themes/megatheme/blueprints.yaml',
    'modified' => 1525360263,
    'data' => [
        'name' => 'MegaTheme',
        'version' => '1.0.0',
        'description' => 'Megakemper own theme modification',
        'icon' => 'crosshairs',
        'author' => [
            'name' => 'Peter Nagy',
            'email' => 'devs@getgrav.org',
            'url' => 'http://getgrav.org'
        ]
    ]
];
