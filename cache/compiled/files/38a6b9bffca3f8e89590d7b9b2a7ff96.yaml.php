<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/peternagy/Devel/revuca/megakemper2019-final/user/plugins/file-content/file-content.yaml',
    'modified' => 1526864238,
    'data' => [
        'enabled' => true,
        'allow_in_page' => true,
        'allow_in_grav' => true,
        'allow_in_filesystem' => false,
        'allowed_extensions' => [
            0 => 'txt',
            1 => 'html'
        ]
    ]
];
