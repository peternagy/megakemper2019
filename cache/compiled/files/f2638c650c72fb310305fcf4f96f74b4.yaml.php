<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://problems/problems.yaml',
    'modified' => 1523527136,
    'data' => [
        'enabled' => true,
        'built_in_css' => true
    ]
];
