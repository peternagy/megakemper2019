<?php

/* partials/header.html.twig */
class __TwigTemplate_2b469ff93541a2bb371c014ea2b3dd746573f3f5acbbfa5d505a621631b3b187 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navigation' => array($this, 'block_navigation'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header id=\"home\">
  ";
        // line 2
        $this->displayBlock('navigation', $context, $blocks);
        // line 5
        echo "  <section class=\"hero\" id=\"hero\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12 text-right navicon\">
          <a id=\"nav-toggle\" class=\"nav_slide_button\" href=\"#\"><span></span></a>
        </div>
      </div>
      <div class=\"row hero-content\">
        <div class=\"col-md-8 col-md-offset-2 text-center inner\">
          <h1 class=\"animated fadeInDown\">";
        // line 14
        echo $this->getAttribute($this->getAttribute(($context["site"] ?? null), "header", array()), "title", array());
        echo "</h1>
          <p class=\"animated fadeInUp delay-05s\">";
        // line 15
        echo $this->getAttribute($this->getAttribute(($context["site"] ?? null), "header", array()), "description", array());
        echo "</p>
          <h3 class=\"desc-headline\">";
        // line 16
        echo $this->getAttribute($this->getAttribute(($context["site"] ?? null), "header", array()), "freereg", array());
        echo "<span>";
        echo $this->env->getExtension('Grav\Plugin\FileContentTwigExtension')->filecontent("/user/plugins/file-content/free.txt");
        echo "</span></h3>
        </div>
      </div>
      <div class=\"row button-row\">
      </div>
      <div class=\"row button-row\">
        <div class=\"col-md-6 col-md-offset-3 text-center\">
          ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["site"] ?? null), "header", array()), "buttons", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
            // line 24
            echo "          <a href=\"";
            echo $this->getAttribute($context["button"], "link", array());
            echo "\" class=\"";
            echo $this->getAttribute($context["button"], "class", array());
            echo "\">";
            echo $this->getAttribute($context["button"], "text", array());
            echo "</a>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "        </div>
      </div>
    </div>
  </section>
</header>
";
    }

    // line 2
    public function block_navigation($context, array $blocks = array())
    {
        // line 3
        echo "  ";
        $this->loadTemplate("partials/navigation.html.twig", "partials/header.html.twig", 3)->display($context);
        // line 4
        echo "  ";
    }

    public function getTemplateName()
    {
        return "partials/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 4,  85 => 3,  82 => 2,  73 => 26,  60 => 24,  56 => 23,  44 => 16,  40 => 15,  36 => 14,  25 => 5,  23 => 2,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header id=\"home\">
  {% block navigation %}
  {% include 'partials/navigation.html.twig' %}
  {% endblock %}
  <section class=\"hero\" id=\"hero\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12 text-right navicon\">
          <a id=\"nav-toggle\" class=\"nav_slide_button\" href=\"#\"><span></span></a>
        </div>
      </div>
      <div class=\"row hero-content\">
        <div class=\"col-md-8 col-md-offset-2 text-center inner\">
          <h1 class=\"animated fadeInDown\">{{ site.header.title }}</h1>
          <p class=\"animated fadeInUp delay-05s\">{{ site.header.description }}</p>
          <h3 class=\"desc-headline\">{{ site.header.freereg }}<span>{{ filecontent('/user/plugins/file-content/free.txt') }}</span></h3>
        </div>
      </div>
      <div class=\"row button-row\">
      </div>
      <div class=\"row button-row\">
        <div class=\"col-md-6 col-md-offset-3 text-center\">
          {% for button in site.header.buttons %}
          <a href=\"{{ button.link }}\" class=\"{{ button.class }}\">{{ button.text }}</a>
          {% endfor %}
        </div>
      </div>
    </div>
  </section>
</header>
", "partials/header.html.twig", "/Users/peternagy/Devel/revuca/megakemper2019-final/user/themes/halcyon/templates/partials/header.html.twig");
    }
}
