<?php

/* modular/ignite.html.twig */
class __TwigTemplate_573e9d0af6bf77f10e14b11e473db4446c91f0831e02807846c01897cbc85995 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"ignite-cta text-center\" id=\"registracia\">
  <div class=\"container\">
    <!-- <div class=\"row\"> -->
      <!-- <div class=\"col\"> -->
        ";
        // line 5
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "
        ";
        // line 6
        $this->loadTemplate("forms/form.html.twig", "modular/ignite.html.twig", 6)->display($context);
        // line 7
        echo "      <!-- </div> -->
    <!-- </div> -->
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "modular/ignite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"ignite-cta text-center\" id=\"registracia\">
  <div class=\"container\">
    <!-- <div class=\"row\"> -->
      <!-- <div class=\"col\"> -->
        {{ page.content }}
        {% include \"forms/form.html.twig\" %}
      <!-- </div> -->
    <!-- </div> -->
  </div>
</div>
", "modular/ignite.html.twig", "/Users/peternagy/Devel/revuca/megakemper2019-final/user/themes/halcyon/templates/modular/ignite.html.twig");
    }
}
