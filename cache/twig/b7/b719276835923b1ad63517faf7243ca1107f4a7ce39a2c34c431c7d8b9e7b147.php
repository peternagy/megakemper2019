<?php

/* partials/footer.html.twig */
class __TwigTemplate_b960596302b3d717fe170cebb89171fa7335c879556ad38b5adba5487903f8ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer>
  <div class=\"container\">
    <div class=\"row\">
    <div class=\"legals\">
      <p><img src=\"/user/themes/megatheme/img/logo.png\" width=\"100\" alt=\"KostolNaHrane\" /></p>
    </div>
  </div>
</div>
</footer>
";
    }

    public function getTemplateName()
    {
        return "partials/footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer>
  <div class=\"container\">
    <div class=\"row\">
    <div class=\"legals\">
      <p><img src=\"/user/themes/megatheme/img/logo.png\" width=\"100\" alt=\"KostolNaHrane\" /></p>
    </div>
  </div>
</div>
</footer>
", "partials/footer.html.twig", "/Users/peternagy/Devel/revuca/megakemper2019-final/user/themes/halcyon/templates/partials/footer.html.twig");
    }
}
