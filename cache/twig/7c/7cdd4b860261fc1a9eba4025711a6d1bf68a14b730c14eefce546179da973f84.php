<?php

/* modular/subscribe.html.twig */
class __TwigTemplate_3c6cb993de78723c3afa6081881e0c690fb49aee6aa33836a40d3c10ab39602c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"subscribe text-center\">
  <div class=\"container\">
    <!-- <h1><i class=\"fa fa-paper-plane\"></i><span>";
        // line 3
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "title", array());
        echo "</span></h1> -->
    <h1><span>";
        // line 4
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "title", array());
        echo "</span></h1>
    <!-- ";
        // line 5
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo " -->

    <div>
    <div class=\"container\">
\t    <div class=\"row\">
\t    \t<div class=\"col-md-4\">
\t    \t\t<ul>
  \t\t\t\t\t<li>spacák</li>
  \t\t\t\t\t<li>karimatku</li>
  \t\t\t\t\t<li>stan</li>
  \t\t\t\t\t<li>teplé oblečenie</li>
  \t\t\t\t\t<li>letné oblečenie</li>
  \t\t\t\t\t<li>tenisky</li>
  \t\t\t\t\t<li>sandále</li>
  \t\t\t\t\t<li>gumáky</li>
  \t\t\t\t\t<li>pevné topánky do lesa</li>
\t\t\t\t</ul>
\t    \t</div>
  \t\t\t<div class=\"col-md-4\">
  \t\t\t\t<ul>
  \t\t\t\t\t<li>pršiplášť</li>
  \t\t\t\t\t<li>ponožky (veľa ponožiek)</li>
  \t\t\t\t\t<li>plavky</li>
  \t\t\t\t\t<li>pokrývku hlavy</li>
  \t\t\t\t\t<li>hygienické potreby</li>
  \t\t\t\t\t<li>baterka/čelovka</li>
  \t\t\t\t\t<li>príbor</li>
  \t\t\t\t\t<li>ešus</li>
  \t\t\t\t\t<li>zápisník</li>
\t\t\t\t</ul>
  \t\t\t</div>
  \t\t\t<div class=\"col-md-4\">
  \t\t\t\t<ul>
  \t\t\t\t\t<li>pero</li>
  \t\t\t\t\t<li>lieky (v prípade ak nejaké užívaš)</li>
  \t\t\t\t\t<li>repelent</li>
  \t\t\t\t\t<li>krém na opaľovanie</li>
  \t\t\t\t\t<li>deku k táboráku</li>
  \t\t\t\t\t<li>ruksak (v prípade výletu)</li>
  \t\t\t\t\t<li>kartu o zdravotnom poistení (nie kópiu)</li>
\t\t\t\t</ul>
  \t\t\t</div>
\t    </div>
\t</div>
\t</div>

  </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/subscribe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"subscribe text-center\">
  <div class=\"container\">
    <!-- <h1><i class=\"fa fa-paper-plane\"></i><span>{{ page.header.title }}</span></h1> -->
    <h1><span>{{ page.header.title }}</span></h1>
    <!-- {{ page.content }} -->

    <div>
    <div class=\"container\">
\t    <div class=\"row\">
\t    \t<div class=\"col-md-4\">
\t    \t\t<ul>
  \t\t\t\t\t<li>spacák</li>
  \t\t\t\t\t<li>karimatku</li>
  \t\t\t\t\t<li>stan</li>
  \t\t\t\t\t<li>teplé oblečenie</li>
  \t\t\t\t\t<li>letné oblečenie</li>
  \t\t\t\t\t<li>tenisky</li>
  \t\t\t\t\t<li>sandále</li>
  \t\t\t\t\t<li>gumáky</li>
  \t\t\t\t\t<li>pevné topánky do lesa</li>
\t\t\t\t</ul>
\t    \t</div>
  \t\t\t<div class=\"col-md-4\">
  \t\t\t\t<ul>
  \t\t\t\t\t<li>pršiplášť</li>
  \t\t\t\t\t<li>ponožky (veľa ponožiek)</li>
  \t\t\t\t\t<li>plavky</li>
  \t\t\t\t\t<li>pokrývku hlavy</li>
  \t\t\t\t\t<li>hygienické potreby</li>
  \t\t\t\t\t<li>baterka/čelovka</li>
  \t\t\t\t\t<li>príbor</li>
  \t\t\t\t\t<li>ešus</li>
  \t\t\t\t\t<li>zápisník</li>
\t\t\t\t</ul>
  \t\t\t</div>
  \t\t\t<div class=\"col-md-4\">
  \t\t\t\t<ul>
  \t\t\t\t\t<li>pero</li>
  \t\t\t\t\t<li>lieky (v prípade ak nejaké užívaš)</li>
  \t\t\t\t\t<li>repelent</li>
  \t\t\t\t\t<li>krém na opaľovanie</li>
  \t\t\t\t\t<li>deku k táboráku</li>
  \t\t\t\t\t<li>ruksak (v prípade výletu)</li>
  \t\t\t\t\t<li>kartu o zdravotnom poistení (nie kópiu)</li>
\t\t\t\t</ul>
  \t\t\t</div>
\t    </div>
\t</div>
\t</div>

  </div>
</section>
", "modular/subscribe.html.twig", "/Users/peternagy/Devel/revuca/megakemper2019-final/user/themes/halcyon/templates/modular/subscribe.html.twig");
    }
}
