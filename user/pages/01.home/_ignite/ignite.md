---
title: registracia
hidemenu: true
cache_enable: false
---

#Registrácia
Po úspešnej registrácii obdržíte email o potvrdení registrácie s platobnými údajmi. Zaslaním registrácie vyjadrujete svoj súhlas so [všeobecnými podmienkami](/user/pages/01.home/_ignite/vseobecnepodmienkyMK2018.pdf) tábora.
