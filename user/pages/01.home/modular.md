---
title: Halcyon
menu: Home
onpage_menu: true
content:
    items: @self.modular
    order:
        by: default
        dir: asc
        custom:
            - _intro
            - _features
            - _services
            - _subscribe
            - _portfolio
            - _swag
            - _ignite
            - _contact
form:
    form:
    name: register
    outerclasses: 'form-group form-inline'
    classes: form-inline
    fields:
        - name: name
          label: Meno a Priezvisko
          autocomplete: on
          type: text
          validate:
            required: true
        - name: birthdate
          label: Dátum narodenia
          autocomplete: on
          type: text
          validate:
            required: true
        - name: address
          label: Adresa
          autocomplete: on
          type: text
          validate:
            required: true
        - name: number
          label: Tel. č.
          autocomplete: on
          type: text
          validate:
            required: true
        - name: email
          label: Email
          type: email
          validate:
            required: true
        - name: invitedby
          label: Kamarát, ktorý ma pozval
          autocomplete: on
          type: text
        - name: message
          label: Choroby, diéta, obmedzenia
          type: textarea
        - name: ocakavania
          label: Moje očakávania od tábora
          type: textarea
        - name: tshirt
          label: Veľkosť trička
          autocomplete: on
          type: select
          size: long
          classes: fancy
          options:
              XS: XS
              S: S
              M: M
              L: L
              XL: XL
              XXL: XXL
          validate:
            required: true
    buttons:
        - type: submit
          outerclasses: 'whatever'
          value: Registrovať
    process:
        - email:
            from: "{{ config.plugins.email.from }}"
            to: "{{ form.value.email }}"
            subject: "[Registácia] {{ form.value.name|e }}"
            body: "Milá/ý {{ form.value.name|e }},<br/><br/>Ďakujeme za registráciu na Megakemper 2019.<br/><br/>Platobné údaje:<br/>IBAN: SK6609000000000511952302<br/>Suma: 50€<br/>Správa pre príjemcu: {{ form.value.name|e }}<br/><br/>Tešíme sa, že budeme môcť s Tebou stráviť pár dní.<br/>Organizačný team Megakemper 2019"
        - email:
            subject: "[Registácia] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - email:
            to: "richard.nagypal@gmail.com"
            subject: "[Registácia] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - save:
            fileprefix: registracia-
            dateformat: Ymd-His-u
            extension: txt
            body: "{% include 'forms/data.txt.twig' %}"
        - message: Ďakujeme za registráciu!
        - reset: true
        - display: /
---
