---
title: Príbeh
portfolio:
  - title: "Ubytovanie"
    img: portfolio-01.jpg
    img_text: Ubytovanie
    img_url: "/#pribeh"
    content: "Spíme v stanoch! Paráda, nie? Preto je dôležité, aby si si zaobstaral stan, (ideálne nie z Tesca), teplý spacák a dostatok teplého oblečenia. Pre dievčatá, ktoré nechcú alebo nemôžu spať v stane, je tu možnost ubytovania v chate."
  - title: "Suchý zákon"
    img: portfolio-02.jpg
    img_text: Suchý zákon
    img_url: "/#pribeh"
    content: "O zábavu bude postarané aj bez alkoholu a cigariet."
    delay: 0.5
  - title: "VEČER OTVORENÝCH DVERÍ"
    img: portfolio-03.jpg
    img_text: VEČER OTVORENÝCH DVERÍ
    img_url: "/#pribeh"
    content: "V utorok 13.8.2019 o 19:30 bude výnimočný večer, kedy sa dvere tábora otvoria pre verejnosť. Tvoju rodičia, priatelia, susedia aj učiteľky budú pozvaní na jeden večer zažiť táborovú atmošku."
    delay: 1

---
