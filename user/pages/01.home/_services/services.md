---
title: Program
slides:
  - title: "Program"
    content: "<p>Ak máš 15-25 rokov, nebojíš sa zablatiť, zamočiť, roztrhať pár starých topánok, ak chceš behať po lese, spievať pri táboráku, spoznať Markétku a zistiť kto ťa stvoril a prečo, divočina MEGAKEMPER bude zaručeným hajlajtom tvojho leta.</p>"
    link_text: ""
    link_url: ""
  - title: "O nás"
    content: "<p>Kemp organizuje kostol na Hrane z Revúcej. </p>Budeš počuť o tom čomu a komu veríme a spoločne budeme premýšľať nad tým, kadiaľ vedie cesta nášho života<p> </p><p>Budeme sa rozprávať o tom, že každý z nás si v sebe nesie svoju dušu, ktorá je cennejšia ako všetky poklady sveta. </p>"
    link_text: ""
    link_url: ""
---
