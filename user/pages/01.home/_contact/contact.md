---
title: Kontakt
boxes:
    -
        icon: map-marker
        title: Adresa
        description: 'Muránska Zdychava (Karafová)'
    -
        icon: mobile
        title: Mobil
        description: '+421 911 868 811'
    -
        icon: paper-plane
        title: Email
        description: '<a href="#">info@megakemper.sk</a>'
social:
    -
        icon: dribbble
        url: '#'
    -
        icon: twitter
        url: '#'
    -
        icon: envelope
        url: '#'
---

# Ak máš otázky, tak nás neváhaj kontaktovať {.arrow}
