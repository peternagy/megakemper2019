---
title: Info
features:
  - title: "KDE?"
    icon: map-marker shadow
    content: "Muránska Zdychava (Karafová)"
    link: "/#info"
    text: "Približne 2km za obcou Muránska Zdychava, chata za potôčikom napravo."  
  - title: "KEDY?"
    icon: calendar shadow
    content: "10.-17. August 2019"
    link: ""
    text: "Začíname 10.8. o 15:00, Končíme 17.8. o 13:00"
    delay: "05"   
  - title: "KOĽKO?"
    icon: credit-card shadow
    content: "50€, IBAN: SK6609000000000511952302"
    link: "/#info"
    text: "dátum splatnosti: 6. 8. 2019 (do poznámky platby uveďte meno účastníka)"
    delay: "1"     
---

# Kľúčové informácie {.arrow}
