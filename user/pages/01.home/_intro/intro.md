---
title: Intro
---

# REGISTRUJ SA, PRÍĎ A ZAŽI {.arrow}

Nad Zdychavskými lazmi zapadlo slnko. V povetrí sa nesie chladná vôňa lesa. Hviezdy sú tu jasnejšie než inde. Sedíš pri vatre, pozeráš do ohňa a spolu s tónmi gitary a zvukmi praskajúceho dreva sa nechávaš unášať k udalostiam dnešného dňa. Si unavený, no usmievaš sa, lebo si nevieš predstaviť lepšie strávený čas. Toto je ochutnávka Zdychavy. Chvíle, ktoré sa dajú v živote prežiť iba raz.

<!-- ![video.mov](video.mov?loop=1&controls=0&autoplay=1&muted=1)
![Sample Image](video.mov?lightbox=600,400&resize=200,200) -->


<!-- [plugin:youtube](https://youtu.be/MS0CE3p6I3k) -->
