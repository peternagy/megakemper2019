---
title: NEZABUDNI SI DONIESŤ!
hidemenu: true

col1: spacák, karimatku, stan, teplé oblečenie, letné oblečenie, tenisky, sandále, gumáky, pevné topánky do lesa, pršiplášť

col2:
	ponožky (veľa ponožiek), plavky, pokrývku hlavy, hygienické potreby, baterka/čelovka, príbor, ešus, zápisník, pero

col3:
	lieky (v prípade ak nejaké užívaš), repelent, krém na opaľovanie, deku k táboráku, ruksak (v prípade výletu), kartu o zdravotnom poistení (nie kópiu)

---

spacák, karimatku, stan
teplé oblečenie
letné oblečenie
tenisky, sandále, gumáky, pevné topánky do lesa
pršiplášť
ponožky (veľa ponožiek)
plavky, pokrývku hlavy 
hygienické potreby
baterka/čelovka
príbor, ešus
zápisník, pero
lieky (v prípade ak nejaké
užívaš) 
repelent, krém na opaľovanie
deku k táboráku 
ruksak (v prípade výletu) 
kartu o zdravotnom poistení (nie kópiu)
